package main

import (
	"time"
	
	"github.com/jmoiron/sqlx"
	"github.com/go-redis/cache"
	"github.com/go-redis/redis"
	"github.com/vmihailenco/msgpack"
)

type PersistentStore struct{
	DB *sqlx.DB
}

func (s PersistentStore) CreatePost(post *Post) error {
	if err := s.DB.NamedExec(CreatePost, post); err != nil {
		return err
	}

	return nil
}

func (s PersistentStore) GetPost(id int) (post *Post, error) {
	if err := s.DB.Get(post, GetPost); err != nil {
		return err
	}

	return nil
}

type CacheStore struct{
	Cache *cache.Codec
}

func NewCacheStore() (*CacheStore, error) {
	store := &CacheStore{}

	client := redis.NewClient(&redis.Options{Addr: ":6379"})
	if _, err := client.Ping().Result(); err != nil {
		return nil, err
	}

	store.Cache = &cache.Codec{
		Redis: client,
		Marshal: func(v interface{}) ([]byte, error) {
			return msgpack.Marshal(v)
		},
		Unmarshal: func(b []byte, v interface{}) error {
			return msgpack.Unmarshal(b, v)
		},
	}
	
	return store, nil	
}

func (s CacheStore) CreatePost(post *Post) error {
	return s.Cache.Set(&cache.Item{
		Key: post.ID,
		Object: post,
		Expiration: time.Hour,
	})
}

type Transactor interface {
	CreatePost(*Post) error
}

type TransactorImpl struct {
	DB    PersistentStore
	Cache CacheStore
}

func (tx Transactor) CreatePost(post *Post) error {
	if err := tx.DB.CreatePost(post); err != nil {
		return err
	}
	go tx.Cache.CreatePost(post)
	
	return nil
}

type Post struct {
	Title string
	ID    int
}

func createPost(tx TransactorImpl, title string) error {
	post := &Post{Title: title, ID: 1}

	if err := tx.CreatePost(post); err != nil {
		return err
	}

	return nil
}

func getPost(tx TransactorImpl, id int) (post *Post, error) {
	if err := tx.Cache.Get(id, post); err != nil {
		if err = tx.DB.GetPost(id); err != nil {
			return nil, err
		}
	}

	return post, nil
}

func main() {

}
